package cymru.prv.ponty;

import org.json.JSONObject;
import org.json.JSONArray;
import twitter4j.*;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Ponty implements Runnable {

    private static final String apiKeysLocation = "api_keys.json";
    private static final String requestURL = "https://api.naturalresources.wales/rivers-and-seas/v1/api/StationData/byLocation?distance=10&lat=53.1371232007899&lon=-3.79721834818175";

    private final List<Double> readings = new LinkedList<>();
    private final Twitter twitter;
    private final JSONObject keys;

    private static final double FLOOD_TRIGGER = 5.672;

    private Status lastStatus;
    private String lastReading = "";
    private boolean isFlood;

    public Ponty() throws IOException {

        keys = new JSONObject(Files.readString(Path.of(apiKeysLocation)));

        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setOAuthConsumerKey(keys.getString("twitter.oauth.consumerkey"))
                .setOAuthConsumerSecret(keys.getString("twitter.oauth.consumersecret"))
                .setOAuthAccessToken(keys.getString("twitter.oauth.accesstoken"))
                .setOAuthAccessTokenSecret(keys.getString("twitter.oauth.accesstokensecret"));

        Configuration config = cb.build();
        twitter = new TwitterFactory(config).getInstance();
        try {
            System.out.println("Twitter Username: " + twitter.getScreenName());
        } catch (TwitterException e) {
            e.printStackTrace();
        }

        ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
        executor.scheduleAtFixedRate(this, 0, 15, TimeUnit.MINUTES);
    }

    @Override
    public void run() {
        try {
            JSONArray data = sendGetRequestToAPI(requestURL);
            if(data.length() < 1)
                return;
            JSONObject pontfawr = data.getJSONObject(0);
            JSONObject params = pontfawr.getJSONArray("parameters").getJSONObject(0);
            double level = params.getDouble("latestValue");
            String read = params.getString("latestTime");
            System.out.printf("Latest reading: %.3f at %s%n", level, read);

            readings.add(0, level);

            if (level < FLOOD_TRIGGER) {
                if (isFlood) {
                    twitter.updateStatus("River level seem to have returned to normal. If you need help or assistance please contact the local authorities");
                } else if (readings.size() >= 3) {
                    if (readings.size() > 3)
                        readings.remove(3);
                    double prediction = linearIncreasePrediction(readings);
                    if (prediction >= FLOOD_TRIGGER) {
                        twitter.updateStatus(String.format(Files.readString(Path.of("prediction.text")), level));
                    }
                }
                isFlood = false;
            } else if (isFlood) {
                if (!lastReading.equals(read)) {
                    lastReading = read;
                    StatusUpdate update = new StatusUpdate("@" + twitter.getScreenName() + " " + String.format("Latest reading: %.3f at %s", level, read));
                    update.setInReplyToStatusId(lastStatus.getId());
                    lastStatus = twitter.updateStatus(update);
                }
            } else {
                isFlood = true;
                lastReading = read;
                lastStatus = twitter.updateStatus(String.format(Files.readString(Path.of("tweet.text")), level));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static double linearIncreasePrediction(List<Double> values) {
        double currentIncrease = values.get(0) - values.get(1);
        double lastIncrease = values.get(1) - values.get(2);
        return values.get(0) + ((currentIncrease - lastIncrease) + currentIncrease);
    }

    private JSONArray sendGetRequestToAPI(String urlString) throws IOException {
        URL url = new URL(urlString);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        con.setRequestProperty("Ocp-Apim-Subscription-Key", keys.getString("ocp-apim-sub-key"));

        con.connect();

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuilder content = new StringBuilder();
        while ((inputLine = in.readLine()) != null) {
            content.append(inputLine);
        }
        in.close();
        con.disconnect();

        return new JSONArray(content.toString());
    }

    public static void main(String[] args) {
        try {
            new Ponty();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
